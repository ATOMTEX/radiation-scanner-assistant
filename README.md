# Radiation Scanner Assistant 1.2.8.9   
### The  “Radiation  Scanner  Assistant”  program  is  intended  for  running  on  a smartphone  to  control  and  operate  AT6101C  and  AT6101CM  Backpack-based Radiation Detectors and AT6103 Mobile Radiation Scanning System (hereinafter the device), which are intended for:    
### •   Spectral  radiation  monitoring  of  indoor  and  outdoor  areas  with  geo-referencing,    
### •   Detection  and  identification  of  gamma  radionuclides,  measurement  of ambient gamma radiation dose equivalent rate,
### •   Detection of neutron radiation sources. 

• [Radiation Scanner Assistant_Manual eng.pdf](./RSA-manual/Radiation Scanner Assistant_eng.pdf)   
• [Radiation Scanner Assistant_Manual rus.pdf](./RSA-manual/Radiation Scanner Assistant_rus.pdf)    

[Version history](./VERSION.md)    
[Список версий](./VERSION_RUS.md)    

[DOWNLOAD .apk VERSION - 1.2.8.9](./Radiation Scanner Assistant - v1.2.8.9.apk)

![alt tag](fon2.jpg)
